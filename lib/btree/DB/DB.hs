{-# LANGUAGE RecordWildCards #-}

-- | Database interface
module DB.DB where

import Control.Monad
import Control.Monad.Primitive (PrimMonad (PrimState))
import DB.BTree.BTree (BTree (Null))
import qualified DB.BTree.BTree as B
import DB.BTree.Internal
import Data.Primitive.MVar (newMVar)
import Optics
import Utils.MVar (modifyMVar)
import Utils.Sz

data DB m k v where
  DB ::
    { _insert :: k -> v -> m (),
      _lookup :: k -> m r -> (v -> m r) -> m r,
      _minKey :: m r -> (k -> m r) -> m r
    } ->
    DB m k v

makeLenses ''DB

newDB :: forall m k v. (PrimMonad m, Ord k) => Sz -> m (DB m k v)
newDB maxSz = do
  bt <- newMVar Null
  let _insert :: k -> v -> m ()
      _insert k v = modifyMVar bt (B.insert maxSz k v)

      _lookup :: k -> m r -> (v -> m r) -> m r
      _lookup = undefined

      _minKey :: m r -> (k -> m r) -> m r
      _minKey = undefined
  return DB {..}
