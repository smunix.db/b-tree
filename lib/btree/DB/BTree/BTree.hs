{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RecordWildCards #-}

-- | BTree storage
module DB.BTree.BTree (BTree (Null), insert) where

import Control.Monad ((>=>))
import Control.Monad.Primitive
import DB.BTree.Internal
import DB.BTree.KV
import Data.Coerce (coerce)
import Data.Kind (Type)
import Data.Primitive (copyMutableArray, newArray, readArray, writeArray)
import Optics
import Utils.Arr
import Utils.KV
import Utils.Sz

{- B-Tree mapping keys (k) to values (v) -}
data BTree (m :: Type -> Type) k v where
  Null :: BTree m k v
  Leaf ::
    { _sz :: !Sz,
      _leafArr :: !(MArr m (KV k v))
    } ->
    BTree m k v
  Node ::
    { _sz :: !Sz,
      _nodeTr0 :: !(BTree m k v),
      _nodeArr :: !(MArr m (KV k (BTree m k v)))
    } ->
    BTree m k v

{-
sizeOf :: BTree m k v -> Sz
sizeOf Null = 0
sizeOf Leaf {_sz = sz} = sz
sizeOf Node {_sz = sz} = sz
-}

minKey_ :: forall m k v. (PrimMonad m) => BTree m k v -> m k
minKey_ Leaf {_leafArr = arr} = readArray arr 0 <&> view k
minKey_ Node {..} = minKey_ _nodeTr0
minKey_ _ = error "minKey is not implemented"

data Signal m k v
  = Ok !(BTree m k v)
  | Split !(BTree m k v) !(BTree m k v)

insert ::
  forall m k v.
  (PrimMonad m, Ord k) =>
  Sz ->
  k ->
  v ->
  BTree m k v ->
  m (BTree m k v)
insert maxSz k v =
  insert' >=> \case
    Ok bt -> return bt
    Split _nodeTr0 rarr -> do
      let _sz = 1
      minK <- minKey_ rarr
      _nodeArr <- newArray (coerce _sz) (KV minK rarr)
      return Node {..}
  where
    insert' :: BTree m k v -> m (Signal m k v)
    insert' Null = do
      _leafArr <- newArray (coerce maxSz) undefined
      writeArray _leafArr 0 (KV k v)
      return $ Ok Leaf {_sz = 1, ..}
    insert' leaf@Leaf {_sz = sz@(coerce -> szI), _leafArr = arr} = search arr sz (\(KV (compare k -> b) _) -> b) found notFound
      where
        szI' :: Int
        szI' = szI `div` 2

        sz' :: Sz
        sz' = coerce szI'

        kv :: (KV k v)
        kv = KV k v

        found :: Int -> m (Signal m k v)
        found i = do
          writeArray arr i kv
          return $ Ok leaf

        notFound :: Int -> m (Signal m k v)
        notFound i =
          if
              | sz + 1 <= maxSz -> do
                writeArray arr (szI + 1) kv
                return $ Ok (leaf {_sz = sz + 1})
              | i < szI' -> do
                -- split leaf, then insert to left
                larr <- newArray (szI' + 1) undefined
                copyMutableArray larr 0 arr 0 i
                writeArray larr i kv
                copyMutableArray larr (i + 1) arr i (szI' - i)
                -- copy right unchanged
                rarr <- newArray szI' undefined
                copyMutableArray rarr 0 arr szI' szI'

                return $ Split (Leaf (sz' + 1) larr) (Leaf sz' rarr)
              | otherwise -> do
                -- copy left unchanged
                larr <- newArray szI' undefined
                copyMutableArray larr 0 arr 0 szI'
                -- split this leaf and insert right
                rarr <- newArray (szI' + 1) undefined
                copyMutableArray rarr 0 arr szI' (i - szI')
                writeArray rarr (i - szI') kv
                copyMutableArray rarr (i - szI' + 1) arr i (szI' - i)
                return $ Split (Leaf sz' larr) (Leaf (sz' + 1) rarr)
    insert' Node {} = error "not implemented"
