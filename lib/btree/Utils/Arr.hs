-- |
module Utils.Arr where

import Control.Arrow
import Control.Monad.Primitive (PrimMonad (PrimState))
import Data.Coerce (coerce)
import Data.Primitive (MutableArray, readArray)
import Utils.KV
import Utils.Sz

{- Mutable array -}
type MArr m a = MutableArray (PrimState m) a

search ::
  forall m a r.
  PrimMonad m =>
  MArr m a ->
  Sz ->
  (a -> Ordering) ->
  (Int -> m r) ->
  (Int -> m r) ->
  m r
search arr (coerce -> sz) cmpFn found notFound = go 0
  where
    go :: Int -> m r
    go !i
      | i >= sz = notFound i
      | otherwise =
        readArray arr i
          >>= ( cmpFn >>> \case
                  GT -> go (i + 1)
                  EQ -> found i
                  LT -> notFound i
              )
