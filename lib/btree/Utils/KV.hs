-- |
module Utils.KV where

import Optics

{- Strict key-value pair -}
data KV k v = KV
  { _k :: !k,
    _v :: !v
  }

makeLenses ''KV
