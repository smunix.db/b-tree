-- |
module Utils.Sz where

{- Size (type safe) -}
newtype Sz = Sz Int
  deriving (Eq, Ord, Show, Bounded)
  deriving (Num, Enum) via Int
