-- |
module Utils.MVar where

import Control.Monad ((>=>))
import Control.Monad.Primitive (PrimMonad (PrimState))
import Data.Primitive.MVar (MVar, putMVar, readMVar)
import Optics

modifyMVar :: (PrimMonad m) => MVar (PrimState m) a -> (a -> m a) -> m ()
modifyMVar mv fn = mv & (readMVar >=> fn >=> putMVar mv)
